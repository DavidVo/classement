<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
			integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<title>A single concurrent</title>
		<style type="text/css">
			*, *:after, *:before { box-sizing: border-box; }
			body {
				margin: 0;
			}
			
			#container {
	  			position: relative;
	 			min-height: 100vh;
	 			padding-bottom: 2.5rem;
				}
				
			header {
				display: inline-block;
				background-color: #ccc;
				position: absolute;
	 			top: 0;
	  			width: 100%;
	  			height: 2rem;
			}
			 
			footer {
				background-color: #ccc;
				position: absolute;
				bottom:0;
	  			width: 100%;
	  			height: 4rem;
			}
	</style>
	</head>
<body>
	<%@ include file="header.jsp"%>
	<div class=container>
		<h1>D�tail du concurrent</h1>
		
		<%@page import="java.io.IOException" %>
		<%@page import="java.io.PrintWriter" %>
		<%@page import="java.util.ArrayList" %>
		<%@page import="java.util.List" %>
		<%@page import="javax.servlet.ServletException" %>
		<%@page import="javax.servlet.annotation.WebServlet" %>
		<%@page import="javax.servlet.http.HttpServlet" %>
		<%@page import="javax.servlet.http.HttpServletRequest" %>
		<%@page import="javax.servlet.http.HttpServletResponse" %>
		<%@page import="com.classement.business_object.Concurrent" %>
		<%@page import="com.classement.data_access_object.ClassementConcurrentDAO" %>

		<%int numDos = Integer.valueOf(request.getParameter("nodossard"));%>
		<%Concurrent c = ClassementConcurrentDAO.trouveConcurrentParNo(numDos);%>
		
		
		
		<p>Nom:  <%= c.getMom()%></p>
		<p>Prenom: <%= c.getPrenom()%></p>
		<p>Temps: <%= c.getTemps()%></p>
		<p>N�Dossard: <%= c.getNoDossard()%></p>
		<br/>
		<p><a href="http://localhost:8080/Classement/SupprimeConcurrent?nodossard=<%= c.getNoDossard()%>">Supression du concurrent</a></p>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>