<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<title>Welcome to our second list</title>
	<style type="text/css">
		*, *:after, *:before { box-sizing: border-box; }
		body {
			margin: 0;
		}
		
		#container {
  			position: relative;
 			min-height: 100vh;
 			padding-bottom: 2.5rem;
			}
			
		header {
			display: inline-block;
			background-color: #ccc;
			position: absolute;
 			top: 0;
  			width: 100%;
  			height: 2rem;
		}
		 
		footer {
			background-color: #ccc;
			position: absolute;
			bottom:0;
  			width: 100%;
  			height: 4rem;
		}
	</style>
	</head>
	<body>
	<%@ include file="header.jsp"%>
	<% if(session.getAttribute("CoptInt") != null) {%>
		<% int compteurInteraction = (int) session.getAttribute("CoptInt");%>
		<%compteurInteraction = compteurInteraction+1;%>
		<% session.setAttribute("CoptInt", compteurInteraction);%>
	<%} %>
	<div class="container text-center">
		<h1 class="text-center">List des concurents</h1>
		<%@page import="java.io.IOException" %>
		<%@page import="java.io.PrintWriter" %>
		<%@page import="java.util.ArrayList" %>
		<%@page import="java.util.List" %>
		<%@page import="javax.servlet.ServletException" %>
		<%@page import="javax.servlet.annotation.WebServlet" %>
		<%@page import="javax.servlet.http.HttpServlet" %>
		<%@page import="javax.servlet.http.HttpServletRequest" %>
		<%@page import="javax.servlet.http.HttpServletResponse" %>
		<%@page import="com.classement.business_object.Concurrent" %>
		<%@page import="com.classement.data_access_object.ClassementConcurrentDAO" %>
		
		<%-- Connection � la base de donn�es --%>
		
		<jsp:useBean id="dbConnexion" class="com.classement.dao.DBConnexion" scope="session"/>
		<%--
		<% List<Concurrent> lConcurrent = new ArrayList<Concurrent>(); %>
		<% lConcurrent = ClassementConcurrentDAO.getTousLesConcurrents(); %>
		--%>
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
		
		<form action="unConcurrent.jsp" method="post">
			<table class="table text-center">
				<tr>
					<td>Nom</td>
					<td>N� Dossard</td>
				</tr>
				<core:forEach items="<%=ClassementConcurrentDAO.getTousLesConcurrents()%>" var="con">
					<tr>
						<td><a href="http://localhost:8080/Classement/unConcurrent.jsp?nodossard=${con.noDossard}"> <core:out value="${con.mom}"/> </a></td>
						<td><core:out value="${con.noDossard}"/> </td>
					</tr>
				</core:forEach>
				<%-- 
				<% for(Concurrent eachConcurrent:lConcurrent) { %>
				
					<td><a href="http://localhost:8080/Classement/unConcurrent.jsp?nodossard=<%= eachConcurrent.getNoDossard()%>"> <%= eachConcurrent.getMom()%> </a></td>
					<td><%=eachConcurrent.getNoDossard()%> </td>
				
				<%} %>
				--%>
			</table>
		</form>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>