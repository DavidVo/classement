
	<%@ page language="java" %>
	<%@ page import="java.time.LocalDateTime" %>
	<%@ page import="java.time.format.DateTimeFormatter" %>
	<% LocalDateTime dateNow = LocalDateTime.now(); %>
	<% DateTimeFormatter formatterJour = DateTimeFormatter.ofPattern("dd-MM-yyyy"); %>
	<% DateTimeFormatter formatterHeure = DateTimeFormatter.ofPattern("HH:mm"); %>
	<% String jourFormatted = dateNow.format(formatterJour); %>
	<% String heureFormatted = dateNow.format(formatterHeure);%>
	<% String headerJour = "Nous sommes le " + jourFormatted + " et il est " + heureFormatted;%>
	<footer class="container"> 
		Bonjour ${sessionScope["pseudo"]}  vous avez vist� <%=session.getAttribute("CoptInt") %> pages <br/>
		<%= headerJour%>
	</footer>
