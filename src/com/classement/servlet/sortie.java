package com.classement.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class sortie
 */
@WebServlet(name="sortie", urlPatterns="/sortie")
public class sortie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public sortie() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getSession().removeAttribute("pseudo"); // suppression du pseudo valid� s'il existe
		request.getSession().setAttribute("CoptInt", null);
		HttpSession session = request.getSession(false);
		if(session!=null) session.invalidate();
		response.sendRedirect(getServletContext().getContextPath() + "/login.html");
	}
}
