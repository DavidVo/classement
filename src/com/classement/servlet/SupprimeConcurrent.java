package com.classement.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.classement.business_object.Concurrent;
import com.classement.dao.DBConnexion;
import com.classement.data_access_object.ClassementConcurrentDAO;

/**
 * Servlet implementation class SupprimeConcurrent
 */
@WebServlet(name="SupprimeConcurrent", urlPatterns="/SupprimeConcurrent")
public class SupprimeConcurrent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SupprimeConcurrent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int numdos = Integer.valueOf(request.getParameter("nodossard"));
		try {
			Concurrent con = DBConnexion.ParNo(numdos);
			ClassementConcurrentDAO.supprimeConcurrent(con);
	        RequestDispatcher rqd= request.getRequestDispatcher("/Liste2.jsp");
	        rqd.forward(request,response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
