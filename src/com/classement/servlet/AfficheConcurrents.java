package com.classement.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.classement.business_object.Concurrent;
import com.classement.data_access_object.ClassementConcurrentDAO;

/**
 * Servlet implementation class AfficheConcurrents
 */
@WebServlet(name="list",urlPatterns="/liste")
public class AfficheConcurrents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficheConcurrents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		// Verification que le login de ma session a été enregistré
		
		List<Concurrent> lConcurrent = new ArrayList<Concurrent>();
		try {
			lConcurrent = ClassementConcurrentDAO.getTousLesConcurrents();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        response.setContentType("text/html");
        PrintWriter printOut = response.getWriter();
        
        // Vu que j'utilise la methode forward le debut du html est  creer dans le.java content le forward 
        
        //printOut.println("<html lang=\"fr\">");
        //printOut.println("<head>");
       // printOut.println("<meta charset=\"utf-8\">");
        //printOut.println("</head>");
        //printOut.println("<body>");
        printOut.println("<div class=\"container\">");
        printOut.println("<h1>Liste concurrents</h1>");
        printOut.println("<p><a href=\"/Classement/aut/creer2.jsp\">Creer un concurrent</a></p>");
        printOut.println("<table>");
    	printOut.println("<tr>");
    	printOut.println("<td>Nom</td>");
    	printOut.println("<td>Prenom</td>");
    	printOut.println("<td>Temps</td>");
    	printOut.println("<td>N° Dossard</td>");
    	printOut.println("</tr>");
        
        for(Concurrent eachConcurrent:lConcurrent) {
        	printOut.println("<tr>");
        	printOut.println("<td>"+eachConcurrent.getMom() +"</td>");
        	printOut.println("<td>"+eachConcurrent.getPrenom() +"</td>");
        	printOut.println("<td>"+eachConcurrent.getTemps() +"</td>");
        	printOut.println("<td>"+eachConcurrent.getNoDossard() +"</td>");
        	printOut.println("</tr>");
        }
        
        printOut.println("</table>");
        printOut.println("</div>");
        printOut.println("</body>");
        printOut.println("</html>");
        printOut.flush();
        
        
	}


}
