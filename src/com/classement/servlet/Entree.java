package com.classement.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.classement.data_access_object.UsrCompteur;

/**
 * Servlet implementation class Entree
 */
@WebServlet(name="entree", urlPatterns="/entree")
public class Entree extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Entree() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lPseudo = request.getParameter("pseudo");
		String lMotDePasse = request.getParameter("mdp");
		int compteurInteraction = 0;

		if (!lMotDePasse.equalsIgnoreCase(lPseudo))  // si les mots de passe et pseudo sont diff�rents
			{
			request.getSession().removeAttribute("pseudo"); // suppression du pseudo valid� s'il existe
			HttpSession session = request.getSession(false);
			if(session!=null) session.invalidate();
			response.sendRedirect(getServletContext().getContextPath() + "/login.html"); // redirection vers login
			}
		else
			{
		
			// cr�ation de la session et placement d'un attribut pseudo, son exsitence suffit � prouver que la session est valide
			request.getSession().setAttribute("pseudo", lPseudo);
			UsrCompteur.addUsr();
			request.getSession().setAttribute("CoptInt", compteurInteraction);
			
			// ... Cette fa�on de g�rer la session est contestable : en r�alit�, il faudrait mieux utiliser : 
			// request.getSession(true); // pour cr�er la session
			// ... et
			// HttpSession session = request.getSession(false);
			// if(session!=null) session.invalidate();
			
			RequestDispatcher rd = request.getRequestDispatcher(/*getServletContext().getContextPath() +*/"/Liste2.jsp");
			rd.forward(request, response);			
			}
		
	}

}
