package com.classement.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.classement.business_object.Concurrent;
import com.classement.data_access_object.ClassementConcurrentDAO;

/**
 * Servlet implementation class creerConcurrent
 */
@WebServlet(name="creer", urlPatterns="/aut/creer")
public class creerConcurrent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public creerConcurrent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String nom = request.getParameter("mom");
		String prenom = request.getParameter("prenom");
		int temps = Integer.valueOf(request.getParameter("temps"));
		
		Concurrent con = new Concurrent(nom,prenom,temps);
		try {
			ClassementConcurrentDAO.creeConcurrent(con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/** reste de qand on créait les ocncurrents à la main
		Concurrent con1 = new Concurrent("Paul","Toto", 25);
		Concurrent con2 = new Concurrent("Damien","Titi", 34);
		Concurrent con3 = new Concurrent("Antoine","plouf", 72);
		Concurrent con4 = new Concurrent("Sylvain","Bonjour", 125);
		Concurrent con5= new Concurrent("Dylan","xxx", 238);	
		
		ClassementConcurrentDAO.creeConcurrent(con1);
		ClassementConcurrentDAO.creeConcurrent(con2);
		ClassementConcurrentDAO.creeConcurrent(con3);
		ClassementConcurrentDAO.creeConcurrent(con4);
		ClassementConcurrentDAO.creeConcurrent(con5);
		 **/
		
        response.setContentType("text/html");
        PrintWriter printOut = response.getWriter();
        printOut.println("<html lang=\"fr\">");
        printOut.println("<head>");
        printOut.println("<meta charset=\"utf-8\">");
        printOut.println("</head>");
        printOut.println("<body>");
        printOut.println("<div class=\"container\">");
        printOut.println("<h1>Concurrents crees</h1>");
        printOut.println("</div>");
       // printOut.println("</body>");
       // printOut.println("</html>");
       // printOut.flush();
        
        // Avec la méthode forward ne pas fermer les balises </html> pour pouvoir  afficher la suite 
        RequestDispatcher rqd= request.getRequestDispatcher("/Liste2.jsp");
        rqd.forward(request,response);
	}

}
