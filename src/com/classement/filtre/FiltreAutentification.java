package com.classement.filtre;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class FiltreAutentification
 */
@WebFilter("/aut/*") //servletNames= {"entree"}
public class FiltreAutentification implements Filter {

    /**
     * Default constructor. 
     */
    public FiltreAutentification() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		
		HttpServletRequest lHttpReq = (HttpServletRequest) request;
		HttpServletResponse lHttpRep = (HttpServletResponse) response;

		HttpSession session = lHttpReq.getSession();
		
		if(session.getAttribute("pseudo") == null) {
			lHttpRep.sendRedirect(request.getServletContext().getContextPath() + "/login.html");
			
		}
		else
		{
		// fait passer la requ�te le long de la chaine des filtres
		chain.doFilter(request, response);
		}
		

		// pass the request along the filter chain
		
		
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
