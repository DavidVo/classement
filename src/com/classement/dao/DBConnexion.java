package com.classement.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.classement.business_object.Concurrent;
import com.classement.exception.UnknownConcurrentException;
import com.mysql.cj.jdbc.MysqlDataSource;
import com.mysql.cj.jdbc.result.ResultSetMetaData;


public class DBConnexion {
	
	private static Connection conn;

	
	
	public DBConnexion() throws SQLException, IOException, ClassNotFoundException, NamingException{
		
		InitialContext lCtx;
		lCtx = new InitialContext();
		DataSource lDs =(DataSource) lCtx.lookup("java:/comp/env/jdbc/TestDB");
		conn = lDs.getConnection();
		
		Class.forName(com.mysql.jdbc.Driver.class.getName());
		
		MysqlDataSource lSqlDS =null;
		Properties lp = new Properties();
		FileInputStream lFis = null;
		try {
		lFis = new FileInputStream("C:/Eclipse/Classement/WebContent/db.properties");
		lp.load(lFis);
		lSqlDS = new MysqlDataSource();
		lSqlDS.setUrl(lp.getProperty("DB_URL"));
		lSqlDS.setUser(lp.getProperty("DB_USER"));
		lSqlDS.setPassword(lp.getProperty("DB_MDP"));
		} catch (IOException e) {e.printStackTrace();}
		
		/**
		try {
			
		} catch(ClassNotFoundException ex) {
			System.out.println("Impossible de charger le pilote");
		}
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/classementdb","root","");
		**/
		
		conn = lSqlDS.getConnection();
		
		Statement st = conn.createStatement();
		System.out.println(conn);
		st.executeUpdate("CREATE TABLE IF NOT EXISTS concurrent(mom TEXT, prenom TEXT, temps REAL, nbDossard INTEGER PRIMARY KEY AUTO_INCREMENT)");
	}
	
	
	public static void creerConcurrent(Concurrent concurrent) throws SQLException{
		
		String mom;
		String prenom;
		float temps;
		int nbDossard;

		mom=concurrent.getMom();
		prenom=concurrent.getPrenom();
		temps=concurrent.getTemps();
		nbDossard=concurrent.getNoDossard();
		PreparedStatement ps = conn.prepareStatement("INSERT INTO concurrent(mom, prenom, temps) VALUES (?,?,?)");
		
		ps.setString(1, mom);
		ps.setString(2, prenom);
		ps.setFloat(3, temps);
		//ps.setInt(4, nbDossard);
		
		int Create_key = ps.executeUpdate();
		System.out.println(String.format("%d viennent d'etre créées", Create_key));
		
	}
	
	
	public static Concurrent ParNo(int no) throws SQLException {
		
		PreparedStatement ps2 = conn.prepareStatement("SELECT * FROM concurrent WHERE nbDossard = ?");
		ps2.setInt(1,no);
		ResultSet rs2 = ps2.executeQuery();
		
		//Concurrent obj = new Concurrent(rs2.getString("mom"),rs2.getString("prenom"),rs2.getFloat("temps"), rs2.getInt("nbDossard"));
		

		Concurrent toArrayList=new Concurrent(); 
		while (rs2.next()) { 
			toArrayList = new Concurrent(rs2.getString("mom"),rs2.getString("prenom"),rs2.getFloat("temps"), rs2.getInt("nbDossard"));
		   }

		return toArrayList;
	}
	
	public static List<Concurrent> tousLesConc() throws SQLException {
		
		PreparedStatement ps3 = conn.prepareStatement("SELECT * FROM concurrent GROUP BY temps");
		ResultSet rs = ps3.executeQuery();
		
		ArrayList<Concurrent> toArrayList = new ArrayList<>(); 
		while (rs.next()) { 
			Concurrent obj = new Concurrent(rs.getString("mom"),rs.getString("prenom"),rs.getFloat("temps"), rs.getInt("nbDossard"));
			toArrayList.add(obj);
		   }
		
		return toArrayList;
	}
	
	public static void supConc(Concurrent concurrent) throws SQLException {
		
		PreparedStatement ps2 = conn.prepareStatement("DELETE FROM concurrent WHERE nbDossard = ?");
		ps2.setInt(1,(int) concurrent.getNoDossard());
		ps2.execute();
	}
	
	
	
	public void closeDBConnexion() throws SQLException{
		conn.close();
	}



}
