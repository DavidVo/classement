package com.classement.business_object;

import java.io.Serializable;

public class Concurrent implements Serializable {
	private static final long serialVersionUID = -8532746189132861552L;
	
	private String mom;
	private String prenom;
	private float temps; 
	private int noDossard;
	
	public int getNoDossard() {
		return noDossard;
	}
	public void setNoDossard(int noDossard) {
		this.noDossard = noDossard;
	}
	public String getMom() {
		return mom;
	}
	public void setMom(String mom) {
		this.mom = mom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public float getTemps() {
		return temps;
	}
	public void setTemps(float temps) {
		this.temps = temps;
	}
	
	public Concurrent(String no, String pr, float te) {
		this.mom=no;
		this.prenom = pr;
		this.temps=te;
		this.noDossard=0;
	}
	
	public Concurrent(String no, String pr, float te, int dos) {
		this.mom=no;
		this.prenom = pr;
		this.temps=te;
		this.noDossard=dos;
	}
	
	public Concurrent() {
		this(null,null,-1);
	}
	

	

}
