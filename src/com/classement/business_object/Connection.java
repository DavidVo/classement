package com.classement.business_object;

public class Connection {
	
	private int num;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public Connection(int num) {
		this.num = num;
	}
	
	public Connection() {
		num++;
	}

}
