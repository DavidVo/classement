package com.classement.data_access_object;
import java.util.Collections;
import java.util.List;
import java.sql.SQLException;
import java.util.ArrayList; 

import com.classement.business_object.Concurrent;
import com.classement.dao.DBConnexion;
import com.classement.exception.UnknownConcurrentException;

public class ClassementConcurrentDAO {
	
	private static List<Concurrent> concurrents = new ArrayList<Concurrent>();
	private static int noDossard = 1;
	
	private ClassementConcurrentDAO() { }
	/**
	 * G�n�re un num�ro de concurrent et le stocke
	 * @param concurrent
	 * @throws SQLException 
	 */
	public static void creeConcurrent(Concurrent concurrent) throws SQLException {
		
		DBConnexion.creerConcurrent(concurrent);
	}
	/**
	 * Met a jour un concurrent
	 * @throws UnknownConcurrentException quand le concurrent n'existe pas
	 * @param concurrent
	 */
	public static void MAJConcurrent(Concurrent concurrent) {
		int index = trouveIndexDuConcurrentParNo(concurrent.getNoDossard());
		if(index > -1) {
			concurrents.set(index, concurrent);
		} else {
			throw new UnknownConcurrentException(concurrent.getNoDossard());
		}
	}
	
	/**
	 * Retourne un concurrent selon son num�ro de dossard
	 * @throws UnknownConcurrentException quand aucun concurrent en m�moire a ce num�ro
	 * @param no le num�ro de le concurrent
	 * @return un concurrent selon son num�ro
	 * @throws SQLException 
	 */
	public static Concurrent trouveConcurrentParNo(int no) throws SQLException {
		Concurrent con = DBConnexion.ParNo(no);
		return con;
	}
	
	/**
	 * @return la liste de concurrents stock�e en m�moire (inchangeable)
	 * @throws SQLException 
	 */
	public static List<Concurrent> getTousLesConcurrents() throws SQLException {
		
		
		List<Concurrent> tsconc = new ArrayList<Concurrent>();
		tsconc = DBConnexion.tousLesConc();
		return tsconc;
	}
	
	/**
	 * D�truit un concurrent
	 * @param le concurrent � supprimer
	 * @throws SQLException 
	 * @throws UnknownConcurrentException si le concurrent n'existe pas en m�moire
	 */
	public static void supprimeConcurrent(Concurrent concurrent) throws SQLException {
		DBConnexion.supConc(concurrent);
	}
	
	/**
     * D�truit un concurrent
	 * @param le num�ro de dossard du concurrent � supprimer
	 * @throws UnknownConcurrentException si le concurrent n'existe pas en m�moire
	 */
	public static void supprimeConcurrent(int no) {
		int index = trouveIndexDuConcurrentParNo(no);
		if(index > -1) {
			concurrents.remove(index);
		} else {
			throw new UnknownConcurrentException(no);
		}
	}
	
	private static int trouveIndexDuConcurrentParNo(int no) {
		for (int i = 0; i < concurrents.size(); i++) {
			Concurrent concurrent = concurrents.get(i);
			if(concurrent.getNoDossard()== no) {
				return i;
			}
		}
		return -1;
	}
  

}
