package com.classement.data_access_object;

public class UsrCompteur {
	
	private static int usr = 0;
	
	public static int getUsr() {
		return usr;
	}

	public static void setUsr(int usr) {
		UsrCompteur.usr = usr;
	}

	public static void addUsr() {
		usr =usr+1;
	}
}
